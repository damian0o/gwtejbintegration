package pl.damian0o.ejb;

import javax.ejb.Local;

/**
 * User related EJB.
 * 
 * @version $Id$
 * @author damian0o
 */
@Local
public interface LoginBean {
	
	boolean login(String username, String password, String sessionId);
	
	boolean logout(String sessionId);

}
