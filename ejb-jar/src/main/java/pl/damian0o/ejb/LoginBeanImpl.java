package pl.damian0o.ejb;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.faces.bean.ApplicationScoped;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Bean for logging with application scoped.
 * 
 * @version $Id$
 * @author damian0o
 */
@Stateless
@ApplicationScoped
public class LoginBeanImpl implements LoginBean {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	String sessionId = null;

	@Resource
	private SessionContext sessionContext;

	@Override
	public boolean login(String username, String password, String sessionId) {

		logger.info("Trying log in user {} with password {} at session "
				+ sessionId, username, password);
		logger.info("Session context {}", sessionContext);

		if (this.sessionId == null && "damian".compareTo(username) == 0
				&& "haselko".compareTo(password) == 0) {
			this.sessionId = sessionId;
			return true;
		}

		return false;
	}

	@Override
	public boolean logout(String sessionId) {

		logger.info("User with session {} wants to log out", sessionId);

		if (this.sessionId != null) {
			if (this.sessionId.compareTo(sessionId) == 0) {
				this.sessionId = null;
				return true;
			}
		}
		return false;
	}

}
