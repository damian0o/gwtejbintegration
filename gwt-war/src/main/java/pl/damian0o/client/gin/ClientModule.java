package pl.damian0o.client.gin;

import pl.damian0o.client.place.ClientPlaceManager;
import pl.damian0o.client.place.DefaultPlace;
import pl.damian0o.client.place.NameTokens;
import pl.damian0o.client.presenter.MainPagePresenter;
import pl.damian0o.client.presenter.MainPageView;

import com.gwtplatform.mvp.client.gin.AbstractPresenterModule;
import com.gwtplatform.mvp.client.gin.DefaultModule;

public class ClientModule extends AbstractPresenterModule {

	@Override
	protected void configure() {
		install(new DefaultModule(ClientPlaceManager.class));

		bindPresenter(MainPagePresenter.class, MainPagePresenter.MyView.class,
				MainPageView.class, MainPagePresenter.MyProxy.class);

		bindConstant().annotatedWith(DefaultPlace.class).to(NameTokens.main);
	}
}
