package pl.damian0o.client.presenter;

import pl.damian0o.client.Messages;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CaptionPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.ViewImpl;

public class MainPageView extends ViewImpl implements MainPagePresenter.MyView {

	Messages messages = GWT.create(Messages.class);

	private TextBox username = new TextBox();
	private TextBox password = new TextBox();

	private Button loginButton = new Button();
	private Button logoutButton = new Button();

	private final VerticalPanel panel;

	@Inject
	public MainPageView() {
		panel = new VerticalPanel();
		
		panel.add(getLoginPanel());
	}

	@Override
	public Widget asWidget() {
		return panel;
	}

	private Widget getLoginPanel() {

		loginButton.setText(messages.ok());

		logoutButton.setText(messages.logout());
		

		CaptionPanel cp = new CaptionPanel(messages.login());

		HorizontalPanel panel = new HorizontalPanel();

		VerticalPanel hp1 = new VerticalPanel();

		hp1.add(new Label(messages.username()));
		hp1.add(new Label(messages.password()));

		panel.add(hp1);

		VerticalPanel hp2 = new VerticalPanel();

		hp2.add(username);
		hp2.add(password);

		HorizontalPanel hp3 = new HorizontalPanel();

		hp3.add(loginButton);
		hp3.add(logoutButton);

		hp2.add(hp3);

		panel.add(hp2);

		cp.add(panel);

		cp.getElement().getStyle().setWidth(350, Unit.PX);

		return cp;
	}

	@Override
	public HasClickHandlers getLoginButton() {
		return loginButton;
	}

	@Override
	public HasClickHandlers getLogoutButton() {
		return logoutButton;
	}

	@Override
	public String getUserName() {
		return username.getText();
	}

	@Override
	public String getPassword() {
		return password.getText();
	}

}
