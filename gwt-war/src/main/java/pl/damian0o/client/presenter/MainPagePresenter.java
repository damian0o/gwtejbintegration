package pl.damian0o.client.presenter;

import pl.damian0o.client.place.NameTokens;
import pl.damian0o.client.service.LoginService;
import pl.damian0o.client.service.LoginServiceAsync;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.ProxyStandard;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;
import com.gwtplatform.mvp.client.proxy.RevealRootContentEvent;

public class MainPagePresenter extends
		Presenter<MainPagePresenter.MyView, MainPagePresenter.MyProxy> {

	public interface MyView extends View {
		
		String getUserName();
		String getPassword();
		
		HasClickHandlers getLoginButton();
		HasClickHandlers getLogoutButton();
		
	}

	@ProxyStandard
	@NameToken(NameTokens.main)
	public interface MyProxy extends ProxyPlace<MainPagePresenter> {
	}

	private final PlaceManager placeManager;

	@Inject
	public MainPagePresenter(final EventBus eventBus, final MyView view,
			final MyProxy proxy, final PlaceManager placeManager) {
		super(eventBus, view, proxy);

		this.placeManager = placeManager;
	}

	@Override
	protected void revealInParent() {
		RevealRootContentEvent.fire(this, this);
	}

	@Override
	protected void onBind() {
		
		getView().getLoginButton().addClickHandler(new LoginHandler());
		getView().getLogoutButton().addClickHandler(new LogoutHandler());
		
		super.onBind();
	}

	@Override
	protected void onReset() {
		super.onReset();
	}

	class LoginHandler implements ClickHandler {

		private LoginServiceAsync service = GWT.create(LoginService.class);

		@Override
		public void onClick(ClickEvent event) {
			service.login(getView().getUserName(), getView().getPassword(),
					new AsyncCallback<String>() {

						@Override
						public void onSuccess(String arg0) {
							Window.alert(arg0);
						}

						@Override
						public void onFailure(Throwable arg0) {
							Window.alert("Failure " + arg0.getMessage());
						}
					});
		}
	}

	class LogoutHandler implements ClickHandler {
		private LoginServiceAsync service = GWT.create(LoginService.class);

		@Override
		public void onClick(ClickEvent event) {
			service.logout(new AsyncCallback<String>() {

				@Override
				public void onSuccess(String arg0) {
					Window.alert(arg0);
				}

				@Override
				public void onFailure(Throwable arg0) {
					Window.alert("Failure " + arg0.getMessage());
				}
			});
		}
	}
}
