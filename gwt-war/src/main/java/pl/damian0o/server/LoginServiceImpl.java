package pl.damian0o.server;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import pl.damian0o.client.service.LoginService;
import pl.damian0o.ejb.LoginBean;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

/**
 * The server side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class LoginServiceImpl extends RemoteServiceServlet implements
		LoginService {
	
	@EJB
	LoginBean loginBean;

	@Override
	public String login(String email, String password) {
		
		HttpServletRequest request = this.getThreadLocalRequest();

		HttpSession session = request.getSession();
		
		if(loginBean.login(email, password, session.getId())){
			return "HI";
		}
		
		return "I it imposible to log in";
	}

	@Override
	public String logout() {
		HttpServletRequest request = this.getThreadLocalRequest();

		HttpSession session = request.getSession();
		
		if(loginBean.logout(session.getId())){
			return "BYE";
		}
		
		return "I it imposible to log out";
	}

}
