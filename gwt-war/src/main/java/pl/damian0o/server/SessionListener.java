package pl.damian0o.server;

import javax.ejb.EJB;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import pl.damian0o.ejb.LoginBean;


public class SessionListener implements HttpSessionListener {

	@EJB
	private LoginBean loginBean;
	
	@Override
	public void sessionCreated(HttpSessionEvent arg0) {
		
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent event) {
		HttpSession session = event.getSession();		
		loginBean.logout(session.getId());
	}
	
}
